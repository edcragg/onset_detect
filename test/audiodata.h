/*
 * A class to read audio data and provide samples
 * one at a time.
 */

#ifndef _H_TEST_AUDIODATA
#define _H_TEST_AUDIODATA

#include <vector>

using namespace std;

template <class SF>
class audioData {
	vector<SF>	audioBuf;
	unsigned int	curSample = 0;

public:
	int		channels = -1;
	int		sampleRate = -1;
	unsigned int	frames = 0;

	SF		getSample(void);
	SF		getSample(unsigned int index);

	void		loadFile(char *filePath);
};

#endif // ifndef _H_TEST_AUDIODATA
