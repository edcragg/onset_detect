#include <string>
#include <iostream>

#include <sndfile.hh>

#include "audiodata.h"

template <class SF>
void audioData<SF>::loadFile(char *filePath)
{
	SNDFILE* sndfile;
	SF_INFO  info;

	sndfile = sf_open(filePath, SFM_READ, &info);

	/* Update class audio format information */
	sampleRate = info.samplerate;
	channels = info.channels;

	float *data = new float[info.frames * info.channels];
	sf_readf_float(sndfile, data, info.frames);
	// TODO: error handling

	/* Read the WAV file one float at a time, taking only the first channel
	 * in multi-channel files */
	frames = 0;
	for (int i = 0; i < info.frames * info.channels; i += info.channels)
	{
		audioBuf.push_back(data[i]);
		frames++;
	}
	// TODO: Mix channels

	delete[] data;
	sf_close(sndfile);

	cout << "Read " << frames << " frames from file." << endl;
}
// Debug prints

template <class SF>
SF audioData<SF>::getSample(void)
{
	SF ret;

	if (curSample < frames) {
		ret = audioBuf[curSample];
		curSample++;
		return ret;
	} else {
		// raise error
	}

	return -10;
}

template <class SF>
SF audioData<SF>::getSample(unsigned int index)
{
	SF ret;

	if (index < frames) {
		ret = audioBuf[index];
		curSample = index + 1;
		return ret;
	} else {
		// raise error
	}

	return -10;
}

/* Trigger compilation for templates, allowing separate .h and .cpp files,
 * rather than header only implementation */
template class audioData<float>;
