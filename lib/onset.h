#ifndef _H_ONSET
#define _H_ONSET

#include "ringbuf.hpp"

template <class SF>
class OnsetDetect
{
	/* derivative detection method */
	SF	getEnergy(SF sample);		/* get audio energy */
	SF	getDerivative(SF sample);	/* get derivative */
	bool	processSampleDeriv(SF sample);	/* detection function */

	/* peak detection */
	RingBuf<SF> avgBuf;			/* averaging buffer */
	int	detectionState = 0;		/* detection state variable */
	int	nonPeakSamples = 0;		/* non-peak sample count */
	int	maxPeak = 0;			/* maximum peak detected */

	bool	peakDetect(SF sample);		/* detect peaks */

public:
	int	sampleRate;
	int	peakWindowSamples;		/* averaging window length */
	int	retriggerSamples;		/* refinement window length */
	int	method;				/* detection method used */

	SF	lastSample = 0;

	bool	processSample(SF sample);	/* process a new sample */

	OnsetDetect(int _sampleRate, float sPeakWindowTime, int msRetrigger, int _method);
};

#define ONSET_DERIV	0
#define ONSET_SPECTRAL	1

#define ONSET_NO_DETECTION	0
#define ONSET_INITIAL_TRIGGER	1

/* Runtime options */

#define ONSET_INITIAL_TRIGGER_THRESH 1024

/* Debugging options */

#if 1	/* change to 1 to output calculated values to a file */
#define DEBUG_ONSET_DERIV
#endif

#endif // ifndef _H_ONSET
