#include <iostream>
#include <type_traits>

#include "onset.h"

#ifdef DEBUG_ONSET_DERIV
#include <fstream>
#endif

using namespace std;

/*
 * processSample()
 *
 * Process a sample, must be called for each new sample obtained
 *
 */
template <typename SF>
bool OnsetDetect<SF>::processSample(SF sample)
{
	int ret;

	switch (method)	{
	case ONSET_DERIV:
		ret = processSampleDeriv(sample);
		break;
	}

	return ret;
}

/*
 * getEnergy()
 *
 * Calculate the energy for a given sample value
 *
 */
template <typename SF>
SF OnsetDetect<SF>::getEnergy(SF sample)
{
	return sample * sample;
}

/*
 * getDerivative()
 *
 * Calculate the derivative of a given sample value, automatically storing
 * the previous sample value.
 *
 */
template <typename SF>
SF OnsetDetect<SF>::getDerivative(SF sample)
{
	SF ret = sample - lastSample;
	lastSample = sample;

	/* Discard negative derivatives, we're not
	 * interested in falling energy */
	if (ret < 0)
		ret = 0;

	return ret;
}

/*
 * peakDetect()
 *
 * Detect peaks in the decision function, return true when a peak has been
 * found.
 *
 */
template <typename SF>
bool OnsetDetect<SF>::peakDetect(SF val)
{
	bool ret = false;

	switch (detectionState)
	{

	case ONSET_NO_DETECTION:	/* initial detection */

		if (avgBuf.avg > 0 &&
		    val > avgBuf.avg * ONSET_INITIAL_TRIGGER_THRESH)
		{
			maxPeak = val;
			detectionState = ONSET_INITIAL_TRIGGER;
		}
		break;

	case ONSET_INITIAL_TRIGGER:	/* Peak refinement */

		if (val > maxPeak)
			maxPeak = val;
		else
			nonPeakSamples++;

		if (nonPeakSamples >= retriggerSamples)
		{
			detectionState = ONSET_NO_DETECTION;
			nonPeakSamples = 0;
			ret = true;
		}

		break;

	}

	avgBuf.add(val);

	return ret;
}

/*
 * processSampleDeriv()
 *
 * Private method triggering all steps required for derivative-based onset
 * detection.
 *
 */
template <typename SF>
bool OnsetDetect<SF>::processSampleDeriv(SF sample)
{
	bool ret;

	#ifdef DEBUG_ONSET_DERIV
	float result;
	ofstream outfile;
	#endif

	SF dt = getDerivative(getEnergy(sample));
	ret = peakDetect(dt);

	#ifdef DEBUG_ONSET_DERIV
	result = 0;
	if (ret) result = 0.5; // Used to plot vertical line

	outfile.open("/tmp/b.dat", ofstream::out | ofstream::app);
	outfile << dt << "\t" << avgBuf.avg << "\t" << result << endl;
	outfile.close();
	#endif

	return ret;
}

/*
 * OnsetDetect class constructor
 *
 */
template <typename SF>
OnsetDetect<SF>::OnsetDetect(int _sampleRate, float sPeakWindowTime,
			     int msRetrigger, int _method)
{
	method = _method;
	sampleRate = _sampleRate;

	peakWindowSamples = _sampleRate * sPeakWindowTime;
	retriggerSamples = _sampleRate * (msRetrigger / 1000.0);

	avgBuf.resize(peakWindowSamples);

	#ifdef DEBUG_ONSET_DERIV
	/* clear output file */
	ofstream outfile;
	outfile.open("/tmp/b.dat", ofstream::out);
	outfile << "" << endl;
	outfile.close();
	#endif
}


/* Trigger compilation for templates, allowing separate .h and .cpp files,
 * rather than header only implementation */
template class OnsetDetect<float>;
