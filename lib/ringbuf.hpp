#ifndef _H_RBUFF
#define _H_RBUFF

#include <vector>

using namespace std;

template <class DF>
class RingBuf {
	int write_pos = 0;		/* next position to write to */
	vector<DF> buf;			/* the buffer */
	vector<DF> var;			/* deviation from the mean */

public:
	int size = 0;			/* ring buffer size */
	int fill = 0;			/* number of filled elements */
	DF sum = 0;			/* rolling stats */
	DF avg = 0;
	DF dev_sum = 0;
	DF dev = 0;

	void add(DF val);
	void resize(int buffer_size);
};


/*
 * RingBuf::add()
 *
 * Add a pixel value to the ringbuffer and calculate stats
 *
 */
template <typename DF>
void RingBuf<DF>::add(DF val)
{
	int d;

	/* decrement sums by value being replaced */
	sum     -= buf[write_pos];
	dev_sum -= var[write_pos];

	/* add new value to averaging buffer */
	buf[write_pos] = val;

	/* increment fill counter, if needed */
	if (fill < size)
		fill++;

	/* calculate average for filled length */
	sum += val;
	avg = sum / fill;

	/* add new deviation value */
	d = val - avg;
	var[write_pos] = (d < 0) ? -1 * d : d;

	/* calculate average deviation */
	dev_sum += var[write_pos];
	dev = dev_sum / fill;

	/* increment write position */
	write_pos = (write_pos + 1) % size;
}

/*
 * resize()
 *
 * Resize the ring buffer
 *
 */
template <typename DF>
void RingBuf<DF>::resize(int buffer_size)
{
	size = buffer_size;
	buf.resize(size, 0);
	var.resize(size, 0);
}

#endif // _H_RBUFF
