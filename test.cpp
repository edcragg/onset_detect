/*
 * test.cpp
 *
 * Test utility for audio onset detection library
 *
 * Loads and audio file and reports detected onsets as an absolute time in
 * seconds.
 *
 * Usage:
 *     ./onset_tool <WAV-path>
 *
 * TODO:
 * - Realtime processing time checks
 * - Results validation
 */

#include <iostream>
#include <string>
#include <fstream>
#include <vector>

#include "test/audiodata.h"

#include "lib/onset.h"

using namespace std;

int usage(char *exec)
{
	cout << "Usage:" << endl;
	cout << "    " << exec << " <WAV-path>" << endl;
	exit(1);
}

int main(int argc, char *argv[])
{
	vector<float> results;

	if (argc < 2)
		usage(argv[0]);

	if (!ifstream(argv[1])) {
		cout << argv[0] << ": File not found." << endl;
		exit(1);
	}

	cout << "Processing input: " << argv[1] << endl;

	audioData<float> a;
	a.loadFile(argv[1]);

	OnsetDetect<float> o(a.sampleRate, 0.1, 4, ONSET_DERIV);

	cout << "-- Peak detection --" << endl;
	cout << "Retrigger samples: " << o.retriggerSamples << endl;
	cout << "Averaging window samples: " << o.peakWindowSamples << endl;
	cout << "-- Detected onsets --" << endl;

	ofstream outfile;
	outfile.open("/tmp/a.dat");

	for (int i = 0; i < a.frames; i++)
	{
		float s;
		float time;

		s = a.getSample();
		if (o.processSample(s)) {
			time = ((float) i / a.sampleRate);
			results.push_back(time);
			cout << "* " << time << endl;
		}

		outfile << s << endl;
	}

	outfile.close();

	/* write out results */
	if (argc > 2)
	{
		ofstream resfile;
		resfile.open(argv[2]);
		for(auto const& value: results)
			resfile << value << endl;
		resfile.close();
		cout << "Results written to: '" << argv[2] << "'" << endl;
	}

	cout << "done." << endl;

	return 0;
}
