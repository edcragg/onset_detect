# Onset lib / tool Makefile

CC=g++
CPPFLAGS=-std=c++11

all: CPPFLAGS += -O3
all: onset_tool

debug: CPPFLAGS +=-ggdb -Og -Ilib
debug: onset_tool

CPPFLAGS_LIB +=-fPIC -shared
CPPFLAGS_TEST +=$(shell pkg-config --cflags sndfile)
LFLAGS=$(shell pkg-config --libs sndfile)

SRC_LIB= \
	lib/onset.cpp

SRC_TEST= \
	test.cpp \
	test/audiodata.cpp

deptest:

onset_tool: $(SRC_TEST) $(SRC_LIB)
	$(CC) $(CPPFLAGS) $(SRC_TEST) $(SRC_LIB) -o $@ $(LFLAGS)

plot:
	gnuplot plot.gp

test:


doc:

clean:
	rm -fv onset_tool

#TODO: shared lib build
